#!/bin/sh

BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Libs

WITH_LIBXC=1
WITH_MPI=1

FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS=-O2

XMLF90_VERSION=1.5.4
LIBXC_VERSION=4.2.3
PSML_VERSION=1.1.8
GRIDXC_VERSION=0.8.5
FLOOK_VERSION=0.7.0-23

cd ${BUNDLE_BUILDS}

#-----------------------------
pkg=flook-${FLOOK_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
  cp -p ${BUNDLE_SOURCE}/flook.setup.make ./setup.make;
  make liball
  cp -p libflookall.a ${BUNDLE_INSTALL}/lib
  cp -p flook.mod ${BUNDLE_INSTALL}/include
)

