#!/bin/sh

BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Libs

WITH_LIBXC=1
WITH_MPI=1

FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
CXX_PARALLEL=mpicxx
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS=-O2

XMLF90_VERSION=1.5.4
LIBXC_VERSION=4.2.3
PSML_VERSION=1.1.8
GRIDXC_VERSION=0.8.5
ELSI_VERSION=2.4.1

cd ${BUNDLE_BUILDS}

SCALAPACK_LIBS="-L/opt/scalapack/openmpi-2.1.2--gfortran-7.2.0/lib -lscalapack"
LAPACK_LIBS=-lveclibfort

# These are needed as exported variables in the cmake file
export SCALAPACK_LIBS LAPACK_LIBS

#-----------------------------
pkg=elsi-${ELSI_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 cmake -DCMAKE_TOOLCHAIN_FILE=${BUNDLE_SOURCE}/elsi-2.4.1.cmake \
       -DCMAKE_Fortran_COMPILER=${FC_PARALLEL} \
       -DCMAKE_C_COMPILER=${CC_PARALLEL} \
       -DCMAKE_CXX_COMPILER=${CXX_PARALLEL} \
       -DCMAKE_Fortran_FLAGS="-O0 -mavx" \
       -DCMAKE_INSTALL_PREFIX=${BUNDLE_INSTALL} ..;
        make -j2; make install)

