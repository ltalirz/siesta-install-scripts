#!/bin/sh

BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Libs

WITH_LIBXC=1
WITH_MPI=1

FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS=-O2

XMLF90_VERSION=1.5.4
LIBXC_VERSION=4.2.3
PSML_VERSION=1.1.8
GRIDXC_VERSION=0.8.5

cd ${BUNDLE_BUILDS}

#-----------------------------
pkg=xmlf90-${XMLF90_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 FC=${FC_SERIAL} FCFLAGS=${FFLAGS} ${BUNDLE_BUILDS}/$pkg/configure --prefix=${BUNDLE_INSTALL};
 make -j4; make install)

