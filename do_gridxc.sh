#!/bin/sh

BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Libs

WITH_LIBXC=1
WITH_MPI=1

FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS=-O2

XMLF90_VERSION=1.5.4
LIBXC_VERSION=4.2.3
PSML_VERSION=1.1.8
GRIDXC_VERSION=0.8.5

cd ${BUNDLE_BUILDS}

#LIBXC_ROOT=${BUNDLE_INSTALL}

#-----------------------------
pkg=libgridxc-${GRIDXC_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tgz; cd $pkg;
  mkdir _build_$pkg; cd _build_$pkg;
  cp -p ${BUNDLE_SOURCE}/gridxc.fortran.mk ./fortran.mk ;
  sh ../src/config.sh ;
  LIBXC_ROOT=${LIBXC_ROOT} WITH_LIBXC=1  WITH_MPI=1 PREFIX=${BUNDLE_INSTALL} sh build.sh;
)

