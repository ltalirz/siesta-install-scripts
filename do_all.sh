#!/bin/sh

# There should be a "Tarballs" subdirectory with pre-downloaded tarballs
# and a few configuration files (examples of the latter in the "Config" subdirectory)

# Builds, Install, and Bin subdirectories are created by this script if they do not exist.
# You might want to clean them before using. 

# Set this to the directory where this script is.
BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile

#-------------------------------------
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Install
BUNDLE_BIN=${BUNDLE_ROOT}/Bin

#------------------------- Compilers and options
FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
CXX_PARALLEL=mpicxx
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS="-O2 -g"

#----------------------------------------------------------
# External libraries
#
# If you have libxc already, set LIBXC_ROOT to its installation directory.
# You can then avoid building libxc with this script (see block below)
# Otherwise leave the next line untouched. (safest overall)
LIBXC_ROOT=${BUNDLE_INSTALL}
#
# Set this to your proper paths
#
NETCDF_ROOT=/usr/local/Cellar/netcdf/4.4.1.1_6
SCALAPACK_LIBS="-L/opt/scalapack/openmpi-2.1.2--gfortran-7.2.0/lib -lscalapack"
LAPACK_LIBS=-lveclibfort   # -llapack -lblas   (maybe)
#----------------------------------------------------------

# Nothing should need to be changed below (but YMMV)


XMLF90_VERSION=1.5.4
LIBXC_VERSION=4.2.3
PSML_VERSION=1.1.8
GRIDXC_VERSION=0.8.5
FLOOK_VERSION=0.7.0-23
ELSI_VERSION=2.4.1
SIESTA_VERSION=rel-MaX-1


# These are needed as exported variables in the arch.make file
export BUNDLE_INSTALL FCFLAGS NETCDF_ROOT SCALAPACK_LIBS LAPACK_LIBS LIBXC_ROOT

mkdir -p ${BUNDLE_BUILDS}
mkdir -p ${BUNDLE_INSTALL}
mkdir -p ${BUNDLE_BIN}

cd ${BUNDLE_BUILDS}

#============================ xmlf90

pkg=xmlf90-${XMLF90_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 FC=${FC_SERIAL} FCFLAGS=${FFLAGS} ${BUNDLE_BUILDS}/$pkg/configure --prefix=${BUNDLE_INSTALL};
 make -j4; make install)

#============================ libpsml

pkg=libpsml-${PSML_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 FC=${FC_SERIAL} FCFLAGS=${FFLAGS} ${BUNDLE_BUILDS}/$pkg/configure \
            --with-xmlf90=${BUNDLE_INSTALL} --prefix=${BUNDLE_INSTALL};
 make -j4; make install)

#============================ libxc  (if needed -- Siesta needs 3.0 < libxc < 4.2.X)

pkg=libxc-${LIBXC_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 FC=${FC_SERIAL} FCFLAGS=${FFLAGS} ${BUNDLE_BUILDS}/$pkg/configure --prefix=${BUNDLE_INSTALL};
 make -j4; make install)

#============================ libgridxc

pkg=libgridxc-${GRIDXC_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tgz; cd $pkg;
  mkdir _build_$pkg; cd _build_$pkg;
  cp -p ${BUNDLE_SOURCE}/gridxc.fortran.mk ./fortran.mk ;
  sh ../src/config.sh ;
  LIBXC_ROOT=${LIBXC_ROOT} WITH_LIBXC=1  WITH_MPI=1 PREFIX=${BUNDLE_INSTALL} sh build.sh;
)

#============================ flook library (for Lua functionality)

pkg=flook-${FLOOK_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
  cp -p ${BUNDLE_SOURCE}/flook.setup.make ./setup.make;
  make liball
  cp -p libflookall.a ${BUNDLE_INSTALL}/lib
  cp -p flook.mod ${BUNDLE_INSTALL}/include
)

#============================ ELSI library

pkg=elsi-${ELSI_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
 mkdir _build_$pkg; cd _build_$pkg;
 cmake -DCMAKE_TOOLCHAIN_FILE=${BUNDLE_SOURCE}/elsi-2.4.1.cmake \
       -DCMAKE_Fortran_COMPILER=${FC_PARALLEL} \
       -DCMAKE_C_COMPILER=${CC_PARALLEL} \
       -DCMAKE_CXX_COMPILER=${CXX_PARALLEL} \
       -DCMAKE_Fortran_FLAGS="-O0 -mavx" \
       -DCMAKE_INSTALL_PREFIX=${BUNDLE_INSTALL} .. ;
        make -j2; make install)


#=====================  Build siesta and a few utilities

pkg=siesta-${SIESTA_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
  cd Obj;
  cp -p ${BUNDLE_SOURCE}/siesta.arch.make ./arch.make;
  sh ../Src/obj_setup.sh ;
  make -j2;
  cp -p siesta ${BUNDLE_BIN};
  echo "===> Building some programs in Util..."
  cd ../Util;
  (cd COOP;  make all; cp -p mprop fat spin_texture ${BUNDLE_BIN});
  (cd Denchar/Src;  make;  cp -p denchar ${BUNDLE_BIN});
  (cd Eig2DOS;  make;  cp -p Eig2DOS ${BUNDLE_BIN});
  (cd Bands;  make all;  cp -p eigfat2plot gnubands ${BUNDLE_BIN});
  (cd Vibra/Src;  make fcbuild vibra;  cp -p fcbuild vibra ${BUNDLE_BIN});
  (cd WFS;  make readwfx;  cp -p readwfx ${BUNDLE_BIN});
  (cd Grid;  make grid2cube g2c_ng;  cp -p grid2cube g2c_ng ${BUNDLE_BIN});
  (cd TS/ts2ts;  make ;  cp -p ts2ts ${BUNDLE_BIN});
#  (cd TS/TBtrans;  make ;  cp -p tbtrans ${BUNDLE_BIN});
#  (cd TS/tshs2tshs;  make ;  cp -p tshs2tshs ${BUNDLE_BIN});
  (cd STM/simple-stm;  make all;  cp -p plstm plsts ${BUNDLE_BIN});
  (cd Unfolding/Src;  make ;  cp -p unfold ${BUNDLE_BIN});
  (cd Macroave/Src;  make ;  cp -p macroave ${BUNDLE_BIN});
  (cd VCA;  make ;  cp -p mixps fractional ${BUNDLE_BIN})
)

