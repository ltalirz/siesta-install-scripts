#!/bin/sh

BUNDLE_ROOT=/Users/ag/W/Siesta/QMobile
BUNDLE_SOURCE=${BUNDLE_ROOT}/Tarballs
BUNDLE_BUILDS=${BUNDLE_ROOT}/Builds
BUNDLE_INSTALL=${BUNDLE_ROOT}/Libs
BUNDLE_BIN=${BUNDLE_ROOT}/Bin

FC_PARALLEL=mpif90
CC_PARALLEL=mpicc
FC_SERIAL=gfortran
CC_SERIAL=gcc
FCFLAGS=-O2 -g


SIESTA_VERSION=rel-MaX-1
#
NETCDF_ROOT=/usr/local/Cellar/netcdf/4.4.1.1_6
SCALAPACK_LIBS="-L/opt/scalapack/openmpi-2.1.2--gfortran-7.2.0/lib -lscalapack"
LAPACK_LIBS=-lveclibfort

# These are needed as exported variables in the arch.make file
export BUNDLE_INSTALL FCFLAGS NETCDF_ROOT SCALAPACK_LIBS LAPACK_LIBS

cd ${BUNDLE_BUILDS}

#-----------------------------
# Build siesta and a few utilities
#
pkg=siesta-${SIESTA_VERSION}
(tar xzf ${BUNDLE_SOURCE}/$pkg.tar.gz; cd $pkg;
  cd Obj;
  cp -p ${BUNDLE_SOURCE}/siesta.arch.make ./arch.make;
  sh ../Src/obj_setup.sh ;
  make -j2;
  cp -p siesta ${BUNDLE_BIN};
  echo "===> Building some programs in Util..."
  cd ../Util;
  (cd COOP;  make all; cp -p mprop fat spin_texture ${BUNDLE_BIN});
  (cd Denchar/Src;  make;  cp -p denchar ${BUNDLE_BIN});
  (cd Eig2DOS;  make;  cp -p Eig2DOS ${BUNDLE_BIN});
  (cd Bands;  make all;  cp -p eigfat2plot gnubands ${BUNDLE_BIN});
  (cd Vibra/Src;  make fcbuild vibra;  cp -p fcbuild vibra ${BUNDLE_BIN});
  (cd WFS;  make readwfx;  cp -p readwfx ${BUNDLE_BIN});
  (cd Grid;  make grid2cube g2c_ng;  cp -p grid2cube g2c_ng ${BUNDLE_BIN});
  (cd TS/ts2ts;  make ;  cp -p ts2ts ${BUNDLE_BIN});
#  (cd TS/TBtrans;  make ;  cp -p tbtrans ${BUNDLE_BIN});
#  (cd TS/tshs2tshs;  make ;  cp -p tshs2tshs ${BUNDLE_BIN});
  (cd STM/simple-stm;  make all;  cp -p plstm plsts ${BUNDLE_BIN});
  (cd Unfolding/Src;  make ;  cp -p unfold ${BUNDLE_BIN});
  (cd Macroave/Src;  make ;  cp -p macroave ${BUNDLE_BIN});
  (cd VCA;  make ;  cp -p mixps fractional ${BUNDLE_BIN})
)

